package data

import "time"

type Masons interface {
	New() Masons

	Create(mason Mason) (*Mason, error)
	Update(mason Mason) (*Mason, error)
	Select() ([]Mason, error)
	Get() (*Mason, error)
	Delete() error

	FilterByID(id int64) Masons
}

type Mason struct {
	ID             int64     `db:"id"`
	LodgeID        int64     `db:"lodge_id"`
	Name           string    `db:"name"`
	Rank           string    `db:"rank"`
	Profession     string    `db:"profession"`
	AcceptanceDate time.Time `db:"acceptance_date"`
}
