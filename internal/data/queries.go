package data

type Querier interface {
	SimpleQuery1(n int64) ([]Mason, error)
	SimpleQuery2(n int64, k string) ([]Lodge, error)
	SimpleQuery3(k string) ([]Mason, error)
	SimpleQuery4(n int64) ([]Mason, error)
	SimpleQuery5(n int64) ([]Lodge, error)

	ComplexQuery1(n int64) ([]Mason, error)
	ComplexQuery2(k string) ([]Mason, error)
	ComplexQuery3(n int64) ([]LodgeEvent, error)
}
