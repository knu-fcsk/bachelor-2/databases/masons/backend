package postgres

import (
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
)

type querier struct {
	db *pgdb.DB
}

func NewQuerier(db *pgdb.DB) data.Querier {
	return &querier{db: db}
}

// Find the masons who have attended at least n meetings in lodges.
func (q *querier) SimpleQuery1(n int64) ([]data.Mason, error) {
	result := make([]data.Mason, 0)
	query := `
		SELECT DISTINCT m.*
		FROM masons m
		JOIN events_members em ON m.id = em.mason_id
		GROUP BY m.id
		HAVING COUNT(em.event_id) >= $1;
	`
	if err := q.db.SelectRaw(&result, query, n); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select masons")
		}
	}
	return result, nil
}

// Find the lodges from country k and with the number of members at least n.
func (q *querier) SimpleQuery2(n int64, k string) ([]data.Lodge, error) {
	result := make([]data.Lodge, 0)
	query := `
		SELECT DISTINCT l.*
		FROM lodges l
		JOIN masons m ON m.lodge_id = l.id
		WHERE l.country = $1
		GROUP BY l.id
		HAVING COUNT(m.id) >= $2;
	`
	if err := q.db.SelectRaw(&result, query, k, n); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select lodges")
		}
	}
	return result, nil
}

// Find master's degree masons from country k.
func (q *querier) SimpleQuery3(k string) ([]data.Mason, error) {
	result := make([]data.Mason, 0)
	query := `
		SELECT DISTINCT m.* 
		FROM masons m
		JOIN lodges l ON l.id = m.lodge_id
		WHERE m.rank = 'Master Mason'
		AND l.country = $1;
	`
	if err := q.db.SelectRaw(&result, query, k); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select masons")
		}
	}
	return result, nil
}

// Find the masons who attended at least 1 meeting in the lodge with id n.
func (q *querier) SimpleQuery4(n int64) ([]data.Mason, error) {
	result := make([]data.Mason, 0)
	query := `
		SELECT DISTINCT m.* 
		FROM masons m
		JOIN events_members em ON em.mason_id = m.id
		JOIN lodge_events le ON le.id = em.event_id
		WHERE le.lodge_id = $1
		GROUP BY m.id
		HAVING COUNT(em.event_id) >= 1;
	`
	if err := q.db.SelectRaw(&result, query, n); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select masons")
		}
	}
	return result, nil
}

// Find the lodges where were held more than n events.
func (q *querier) SimpleQuery5(n int64) ([]data.Lodge, error) {
	result := make([]data.Lodge, 0)
	query := `
		SELECT DISTINCT l.*
		FROM lodges l
		JOIN lodge_events le ON le.lodge_id = l.id
		GROUP BY l.id
		HAVING COUNT(le.id) >= $1;
	`
	if err := q.db.SelectRaw(&result, query, n); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select masons")
		}
	}
	return result, nil
}

// Select the masons who attended at least the same events as the mason with id n.
func (q *querier) ComplexQuery1(n int64) ([]data.Mason, error) {
	result := make([]data.Mason, 0)
	query := `
		SELECT DISTINCT m2.*
		FROM masons m1
		JOIN events_members em1 ON em1.mason_id = m1.id
		JOIN lodge_events le ON le.id = em1.event_id
		JOIN events_members em2 ON em2.event_id = le.id
		JOIN masons m2 ON m2.id = em2.mason_id
		WHERE m1.id = $1
		AND NOT EXISTS (
			SELECT le2.id
			FROM masons m3
			JOIN events_members em3 ON em3.mason_id = m3.id
			JOIN lodge_events le2 ON le2.id = em3.event_id
			WHERE m3.id = $1
			AND le2.id NOT IN (
				SELECT em4.event_id
				FROM events_members em4
				WHERE em4.mason_id = m2.id
			)
	);`
	if err := q.db.SelectRaw(&result, query, n); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select masons")
		}
	}
	return result, nil
}

// Select the masons who attended events in at least all lodges of country k.
func (q *querier) ComplexQuery2(k string) ([]data.Mason, error) {
	result := make([]data.Mason, 0)
	query := `
		SELECT DISTINCT m.*
		FROM masons m
		JOIN events_members em ON em.mason_id = m.id
		JOIN lodge_events le ON le.id = em.event_id
		JOIN lodges l ON l.id = le.lodge_id
		WHERE l.country = $1
		AND NOT EXISTS (
			SELECT l2.id
			FROM lodges l2
			WHERE l2.country = $1
			AND l2.id NOT IN (
				SELECT le3.lodge_id
				FROM events_members em2
				JOIN lodge_events le3 ON le3.id = em2.event_id
				WHERE em2.mason_id = m.id
			)
		);
	`
	if err := q.db.SelectRaw(&result, query, k); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select masons")
		}
	}
	return result, nil
}

// Select the events where at least all Lodge n Masons with the 'Fellow Craft' degree were present.
func (q *querier) ComplexQuery3(n int64) ([]data.LodgeEvent, error) {
	result := make([]data.LodgeEvent, 0)
	query := `
		SELECT DISTINCT le.*
		FROM lodge_events le
		WHERE NOT EXISTS (
		 SELECT m2.id
		 FROM masons m2
		 JOIN lodges l2 ON l2.id = m2.lodge_id
		 WHERE m2.rank = 'Fellow Craft'
		 AND l2.id = $1
		 AND m2.id NOT IN (
		  SELECT m3.id 
		  FROM masons m3
		  JOIN events_members em3 ON em3.mason_id = m3.id
		  WHERE em3.event_id = le.id
		 )
		);
`
	if err := q.db.SelectRaw(&result, query, n); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select lodge events")
		}
	}
	return result, nil
}
