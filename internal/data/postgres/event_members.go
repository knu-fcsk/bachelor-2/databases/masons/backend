package postgres

import (
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
)

const (
	eventMembersTable = "event_members"
)

var ErrNoSuchEventMember = errors.New("no such event member")

type eventMembersQ struct {
	db            *pgdb.DB
	selectBuilder sq.SelectBuilder
	updateBuilder sq.UpdateBuilder
	deleteBuilder sq.DeleteBuilder
}

func NewEventMembersQ(db *pgdb.DB) data.EventMembers {
	return &eventMembersQ{
		db:            db.Clone(),
		selectBuilder: sq.Select("*").From(eventMembersTable),
		updateBuilder: sq.Update(eventMembersTable),
		deleteBuilder: sq.Delete(eventMembersTable),
	}
}

func (q *eventMembersQ) New() data.EventMembers {
	return NewEventMembersQ(q.db)
}

func (q *eventMembersQ) Create(member data.EventMember) (*data.EventMember, error) {
	clauses := map[string]interface{}{
		"event_id": member.EventID,
		"mason_id": member.MasonID,
	}
	result := new(data.EventMember)
	query := sq.Insert(eventMembersTable).SetMap(clauses).Suffix("RETURNING *")
	err := q.db.Get(result, query)
	return result, errors.Wrap(err, "failed to create event member")
}

func (q *eventMembersQ) Update(member data.EventMember) (*data.EventMember, error) {
	clauses := map[string]interface{}{
		"event_id": member.EventID,
		"mason_id": member.MasonID,
	}
	result := new(data.EventMember)
	stmt := q.updateBuilder.SetMap(clauses).Suffix("RETURNING *")
	err := q.db.Get(result, &stmt)
	return result, errors.Wrap(err, "failed to update event member")
}

func (q *eventMembersQ) Select() ([]data.EventMember, error) {
	result := make([]data.EventMember, 0)
	if err := q.db.Select(&result, q.selectBuilder); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select event members")
		}
	}
	return result, nil
}

func (q *eventMembersQ) Get() (*data.EventMember, error) {
	result := new(data.EventMember)
	err := q.db.Get(result, q.selectBuilder)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return result, errors.Wrap(err, "failed to get event member")
}

func (q *eventMembersQ) Delete() error {
	result, err := q.db.ExecWithResult(q.deleteBuilder)
	if err != nil {
		return errors.Wrap(err, "failed to delete event member")
	}
	affectedRows, _ := result.RowsAffected()
	if affectedRows == 0 {
		return ErrNoSuchEventMember
	}

	return nil
}

func (q *eventMembersQ) FilterByID(id int64) data.EventMembers {
	q.selectBuilder = q.selectBuilder.Where(sq.Eq{"id": id})
	q.updateBuilder = q.updateBuilder.Where(sq.Eq{"id": id})
	q.deleteBuilder = q.deleteBuilder.Where(sq.Eq{"id": id})

	return q
}
