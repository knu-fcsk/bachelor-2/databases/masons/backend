package postgres

import (
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
)

const (
	lodgeEventsTable = "lodge_events"
)

var ErrNoSuchLodgeEvent = errors.New("no such lodge event")

type lodgeEventsQ struct {
	db            *pgdb.DB
	selectBuilder sq.SelectBuilder
	updateBuilder sq.UpdateBuilder
	deleteBuilder sq.DeleteBuilder
}

func NewLodgeEventsQ(db *pgdb.DB) data.LodgeEvents {
	return &lodgeEventsQ{
		db:            db.Clone(),
		selectBuilder: sq.Select("*").From(lodgeEventsTable),
		updateBuilder: sq.Update(lodgeEventsTable),
		deleteBuilder: sq.Delete(lodgeEventsTable),
	}
}

func (q *lodgeEventsQ) New() data.LodgeEvents {
	return NewLodgeEventsQ(q.db)
}

func (q *lodgeEventsQ) Create(event data.LodgeEvent) (*data.LodgeEvent, error) {
	clauses := map[string]interface{}{
		"lodge_id":    event.LodgeID,
		"name":        event.Name,
		"date":        event.Date,
		"description": event.Description,
	}
	result := new(data.LodgeEvent)
	query := sq.Insert(lodgeEventsTable).SetMap(clauses).Suffix("RETURNING *")
	err := q.db.Get(result, query)
	return result, errors.Wrap(err, "failed to create lodge event")
}

func (q *lodgeEventsQ) Update(event data.LodgeEvent) (*data.LodgeEvent, error) {
	clauses := map[string]interface{}{
		"lodge_id":    event.LodgeID,
		"name":        event.Name,
		"date":        event.Date,
		"description": event.Description,
	}
	result := new(data.LodgeEvent)
	stmt := q.updateBuilder.SetMap(clauses).Suffix("RETURNING *")
	err := q.db.Get(result, &stmt)
	return result, errors.Wrap(err, "failed to update lodgeEvent")
}

func (q *lodgeEventsQ) Select() ([]data.LodgeEvent, error) {
	result := make([]data.LodgeEvent, 0)
	if err := q.db.Select(&result, q.selectBuilder); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select lodge events")
		}
	}
	return result, nil
}

func (q *lodgeEventsQ) Get() (*data.LodgeEvent, error) {
	result := new(data.LodgeEvent)
	err := q.db.Get(result, q.selectBuilder)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return result, errors.Wrap(err, "failed to get lodge event")
}

func (q *lodgeEventsQ) Delete() error {
	result, err := q.db.ExecWithResult(q.deleteBuilder)
	if err != nil {
		return errors.Wrap(err, "failed to delete lodge event")
	}
	affectedRows, _ := result.RowsAffected()
	if affectedRows == 0 {
		return ErrNoSuchLodgeEvent
	}

	return nil
}

func (q *lodgeEventsQ) FilterByID(id int64) data.LodgeEvents {
	q.selectBuilder = q.selectBuilder.Where(sq.Eq{"id": id})
	q.updateBuilder = q.updateBuilder.Where(sq.Eq{"id": id})
	q.deleteBuilder = q.deleteBuilder.Where(sq.Eq{"id": id})

	return q
}
