package postgres

import (
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
)

const (
	lodgesTable = "lodges"
)

var ErrNoSuchLodge = errors.New("no such lodge")

type lodgesQ struct {
	db            *pgdb.DB
	selectBuilder sq.SelectBuilder
	updateBuilder sq.UpdateBuilder
	deleteBuilder sq.DeleteBuilder
}

func NewLodgesQ(db *pgdb.DB) data.Lodges {
	return &lodgesQ{
		db:            db.Clone(),
		selectBuilder: sq.Select("*").From(lodgesTable),
		updateBuilder: sq.Update(lodgesTable),
		deleteBuilder: sq.Delete(lodgesTable),
	}
}

func (q *lodgesQ) New() data.Lodges {
	return NewLodgesQ(q.db)
}

func (q *lodgesQ) Create(lodge data.Lodge) (*data.Lodge, error) {
	clauses := map[string]interface{}{
		"name":    lodge.Name,
		"country": lodge.Country,
	}
	result := new(data.Lodge)
	query := sq.Insert(lodgesTable).SetMap(clauses).Suffix("RETURNING *")
	err := q.db.Get(result, query)
	return result, errors.Wrap(err, "failed to create lodge")
}

func (q *lodgesQ) Update(lodge data.Lodge) (*data.Lodge, error) {
	clauses := map[string]interface{}{
		"name":    lodge.Name,
		"country": lodge.Country,
	}
	result := new(data.Lodge)
	stmt := q.updateBuilder.SetMap(clauses).Suffix("RETURNING *")
	err := q.db.Get(result, &stmt)
	return result, errors.Wrap(err, "failed to update lodge")
}

func (q *lodgesQ) Select() ([]data.Lodge, error) {
	result := make([]data.Lodge, 0)
	if err := q.db.Select(&result, q.selectBuilder); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select lodges")
		}
	}
	return result, nil
}

func (q *lodgesQ) Get() (*data.Lodge, error) {
	result := new(data.Lodge)
	err := q.db.Get(result, q.selectBuilder)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return result, errors.Wrap(err, "failed to get lodge")
}

func (q *lodgesQ) Delete() error {
	result, err := q.db.ExecWithResult(q.deleteBuilder)
	if err != nil {
		return errors.Wrap(err, "failed to delete lodge")
	}
	affectedRows, _ := result.RowsAffected()
	if affectedRows == 0 {
		return ErrNoSuchLodge
	}

	return nil
}

func (q *lodgesQ) FilterByID(id int64) data.Lodges {
	q.selectBuilder = q.selectBuilder.Where(sq.Eq{"id": id})
	q.updateBuilder = q.updateBuilder.Where(sq.Eq{"id": id})
	q.deleteBuilder = q.deleteBuilder.Where(sq.Eq{"id": id})

	return q
}
