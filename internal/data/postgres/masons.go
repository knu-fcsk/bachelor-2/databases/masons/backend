package postgres

import (
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
)

const (
	masonsTable = "masons"
)

var ErrNoSuchMason = errors.New("no such mason")

type masonsQ struct {
	db            *pgdb.DB
	selectBuilder sq.SelectBuilder
	updateBuilder sq.UpdateBuilder
	deleteBuilder sq.DeleteBuilder
}

func NewMasonsQ(db *pgdb.DB) data.Masons {
	return &masonsQ{
		db:            db.Clone(),
		selectBuilder: sq.Select("*").From(masonsTable),
		updateBuilder: sq.Update(masonsTable),
		deleteBuilder: sq.Delete(masonsTable),
	}
}

func (q *masonsQ) New() data.Masons {
	return NewMasonsQ(q.db)
}

func (q *masonsQ) Create(mason data.Mason) (*data.Mason, error) {
	clauses := map[string]interface{}{
		"lodge_id":        mason.LodgeID,
		"name":            mason.Name,
		"rank":            mason.Rank,
		"profession":      mason.Profession,
		"acceptance_date": mason.AcceptanceDate,
	}
	result := new(data.Mason)
	query := sq.Insert(masonsTable).SetMap(clauses).Suffix("RETURNING *")
	err := q.db.Get(result, query)
	return result, errors.Wrap(err, "failed to create mason")
}

func (q *masonsQ) Update(mason data.Mason) (*data.Mason, error) {
	clauses := map[string]interface{}{
		"lodge_id":        mason.LodgeID,
		"name":            mason.Name,
		"rank":            mason.Rank,
		"profession":      mason.Profession,
		"acceptance_date": mason.AcceptanceDate,
	}
	result := new(data.Mason)
	stmt := q.updateBuilder.SetMap(clauses).Suffix("RETURNING *")
	err := q.db.Get(result, &stmt)
	return result, errors.Wrap(err, "failed to update mason")
}

func (q *masonsQ) Select() ([]data.Mason, error) {
	result := make([]data.Mason, 0)
	if err := q.db.Select(&result, q.selectBuilder); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(err, "failed to select masons")
		}
	}
	return result, nil
}

func (q *masonsQ) Get() (*data.Mason, error) {
	result := new(data.Mason)
	err := q.db.Get(result, q.selectBuilder)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return result, errors.Wrap(err, "failed to get user")
}

func (q *masonsQ) Delete() error {
	result, err := q.db.ExecWithResult(q.deleteBuilder)
	if err != nil {
		return errors.Wrap(err, "failed to delete mason")
	}
	affectedRows, _ := result.RowsAffected()
	if affectedRows == 0 {
		return ErrNoSuchMason
	}

	return nil
}

func (q *masonsQ) FilterByID(id int64) data.Masons {
	q.selectBuilder = q.selectBuilder.Where(sq.Eq{"id": id})
	q.updateBuilder = q.updateBuilder.Where(sq.Eq{"id": id})
	q.deleteBuilder = q.deleteBuilder.Where(sq.Eq{"id": id})

	return q
}
