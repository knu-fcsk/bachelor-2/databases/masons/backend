package data

type EventMembers interface {
	New() EventMembers

	Create(member EventMember) (*EventMember, error)
	Update(member EventMember) (*EventMember, error)
	Select() ([]EventMember, error)
	Get() (*EventMember, error)
	Delete() error

	FilterByID(id int64) EventMembers
}

type EventMember struct {
	ID      int64 `db:"id"`
	EventID int64 `db:"event_id"`
	MasonID int64 `db:"mason_id"`
}
