package data

type Lodges interface {
	New() Lodges

	Create(lodge Lodge) (*Lodge, error)
	Update(lodge Lodge) (*Lodge, error)
	Select() ([]Lodge, error)
	Get() (*Lodge, error)
	Delete() error

	FilterByID(id int64) Lodges
}

type Lodge struct {
	ID      int64  `db:"id"`
	Name    string `db:"name"`
	Country string `db:"country"`
}
