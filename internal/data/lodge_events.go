package data

import "time"

type LodgeEvents interface {
	New() LodgeEvents

	Create(event LodgeEvent) (*LodgeEvent, error)
	Update(event LodgeEvent) (*LodgeEvent, error)
	Select() ([]LodgeEvent, error)
	Get() (*LodgeEvent, error)
	Delete() error

	FilterByID(id int64) LodgeEvents
}

type LodgeEvent struct {
	ID          int64     `db:"id"`
	LodgeID     int64     `db:"lodge_id"`
	Name        string    `db:"name"`
	Date        time.Time `db:"date"`
	Description string    `db:"description"`
}
