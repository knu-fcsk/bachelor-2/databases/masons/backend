package parameters

import (
	"net/http"
	"strconv"
)

func FetchNParamFromHeader(r *http.Request) (int64, error) {
	const headerKey = "n"
	value := r.Header.Get(headerKey)
	return strconv.ParseInt(value, 10, 64)
}

func FetchKParamFromHeader(r *http.Request) (string, error) {
	const headerKey = "k"
	value := r.Header.Get(headerKey)
	return value, nil
}
