package responses

import (
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

func ComposeLodge(lodge *data.Lodge) *resources.LodgeResponse {
	return &resources.LodgeResponse{
		Data: *convertToLodgeResource(*lodge),
	}
}
