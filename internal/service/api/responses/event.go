package responses

import (
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

func ComposeEvent(event *data.LodgeEvent) *resources.EventResponse {
	return &resources.EventResponse{
		Data: *convertToEventResource(*event),
	}
}
