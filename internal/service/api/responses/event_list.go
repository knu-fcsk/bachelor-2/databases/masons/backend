package responses

import (
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

func ComposeEventList(events []data.LodgeEvent) *resources.EventListResponse {
	eventResources := make([]resources.Event, len(events))
	for i, event := range events {
		eventResources[i] = *convertToEventResource(event)
	}
	return &resources.EventListResponse{Data: eventResources}
}
