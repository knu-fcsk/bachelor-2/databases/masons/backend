package responses

import (
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

func ComposeMason(mason *data.Mason) *resources.MasonResponse {
	return &resources.MasonResponse{
		Data: *convertToMasonResource(*mason),
	}
}
