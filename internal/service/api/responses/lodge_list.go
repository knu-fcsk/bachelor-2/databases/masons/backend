package responses

import (
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

func ComposeLodgeList(lodges []data.Lodge) *resources.LodgeListResponse {
	lodgesResources := make([]resources.Lodge, len(lodges))
	for i, lodge := range lodges {
		lodgesResources[i] = *convertToLodgeResource(lodge)
	}
	return &resources.LodgeListResponse{Data: lodgesResources}
}
