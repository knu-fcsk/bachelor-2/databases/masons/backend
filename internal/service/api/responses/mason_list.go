package responses

import (
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

func ComposeMasonList(masons []data.Mason) *resources.MasonListResponse {
	masonsResources := make([]resources.Mason, len(masons))
	for i, mason := range masons {
		masonsResources[i] = *convertToMasonResource(mason)
	}
	return &resources.MasonListResponse{Data: masonsResources}
}
