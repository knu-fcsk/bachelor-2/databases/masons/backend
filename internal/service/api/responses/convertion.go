package responses

import (
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

func convertToLodgeResource(lodge data.Lodge) *resources.Lodge {
	return &resources.Lodge{
		Key: resources.NewKeyInt64(lodge.ID, resources.LODGE),
		Attributes: resources.LodgeAttributes{
			Country: lodge.Country,
			Name:    lodge.Name,
		},
	}
}

func convertToMasonResource(mason data.Mason) *resources.Mason {
	return &resources.Mason{
		Key: resources.NewKeyInt64(mason.ID, resources.MASON),
		Attributes: resources.MasonAttributes{
			AcceptanceDate: mason.AcceptanceDate.Unix(),
			LodgeId:        mason.LodgeID,
			Name:           mason.Name,
			Profession:     mason.Profession,
			Rank:           mason.Rank,
		},
	}
}

func convertToEventResource(event data.LodgeEvent) *resources.Event {
	return &resources.Event{
		Key: resources.NewKeyInt64(event.ID, resources.EVENT),
		Attributes: resources.EventAttributes{
			Date:        event.Date.Unix(),
			Description: event.Description,
			LodgeId:     event.LodgeID,
			Name:        event.Name,
		},
	}
}
