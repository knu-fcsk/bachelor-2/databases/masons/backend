package requests

import (
	"encoding/json"
	"net/http"

	ozzo "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/pkg/errors"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/parameters"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

type EditLodgeRequest struct {
	Data    resources.EditLodge
	LodgeID int64
}

func NewEditLodge(r *http.Request) (*EditLodgeRequest, error) {
	request := new(EditLodgeRequest)
	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		return nil, ozzo.Errors{
			"/": errors.Wrap(err, "failed to decode request body"),
		}
	}
	lodgeID, err := parameters.FetchPathID(r)
	if err != nil {
		return nil, ozzo.Errors{
			"path {id}": errors.Wrap(err, "failed to fetch path id"),
		}
	}
	request.LodgeID = lodgeID
	return request, request.validate()

}

func (r *EditLodgeRequest) validate() error {
	return ozzo.Errors{
		"new_country": ozzo.Validate(r.Data.Attributes.NewCountry, ozzo.Length(1, 255)),
		"new_name":    ozzo.Validate(r.Data.Attributes.NewName, ozzo.Length(1, 255)),
	}.Filter()
}
