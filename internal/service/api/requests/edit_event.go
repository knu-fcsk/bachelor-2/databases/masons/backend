package requests

import (
	"encoding/json"
	"net/http"

	ozzo "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/pkg/errors"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/parameters"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

type EditEventRequest struct {
	Data    resources.EditEvent
	EventID int64
}

func NewEditEvent(r *http.Request) (*EditEventRequest, error) {
	request := new(EditEventRequest)
	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		return nil, ozzo.Errors{
			"/": errors.Wrap(err, "failed to decode request body"),
		}
	}
	eventID, err := parameters.FetchPathID(r)
	if err != nil {
		return nil, ozzo.Errors{
			"path {id}": errors.Wrap(err, "failed to fetch path id"),
		}
	}
	request.EventID = eventID
	return request, request.validate()

}

func (r *EditEventRequest) validate() error {
	return ozzo.Errors{
		"new_description": ozzo.Validate(r.Data.Attributes.NewDescription, ozzo.Length(1, 1024)),
		"new_name":        ozzo.Validate(r.Data.Attributes.NewName, ozzo.Length(1, 255)),
	}.Filter()
}
