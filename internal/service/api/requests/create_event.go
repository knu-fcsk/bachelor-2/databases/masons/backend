package requests

import (
	"encoding/json"
	"net/http"

	ozzo "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/pkg/errors"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

type CreateEventRequest struct {
	Data resources.Event
}

func NewCreateEvent(r *http.Request) (*CreateEventRequest, error) {
	request := new(CreateEventRequest)
	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		return nil, ozzo.Errors{
			"/": errors.Wrap(err, "failed to decode request body"),
		}
	}
	return request, request.validate()
}

func (r *CreateEventRequest) validate() error {
	return ozzo.Errors{
		"date":        ozzo.Validate(r.Data.Attributes.Date, ozzo.Required),
		"description": ozzo.Validate(r.Data.Attributes.Description, ozzo.Length(1, 1024)),
		"lodge_id":    ozzo.Validate(r.Data.Attributes.LodgeId, ozzo.Required),
		"name":        ozzo.Validate(r.Data.Attributes.Name, ozzo.Length(1, 255)),
	}.Filter()
}
