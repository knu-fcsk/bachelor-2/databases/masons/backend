package requests

import (
	"encoding/json"
	"net/http"

	ozzo "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/pkg/errors"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

type CreateMasonRequest struct {
	Data resources.Mason
}

func NewCreateMason(r *http.Request) (*CreateMasonRequest, error) {
	request := new(CreateMasonRequest)
	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		return nil, ozzo.Errors{
			"/": errors.Wrap(err, "failed to decode request body"),
		}
	}
	return request, request.validate()
}

func (r *CreateMasonRequest) validate() error {
	return ozzo.Errors{
		"acceptance_date": ozzo.Validate(r.Data.Attributes.AcceptanceDate, ozzo.Required),
		"lodge_id":        ozzo.Validate(r.Data.Attributes.LodgeId, ozzo.Required),
		"name":            ozzo.Validate(r.Data.Attributes.Name, ozzo.Length(1, 255)),
		"profession":      ozzo.Validate(r.Data.Attributes.Profession, ozzo.Length(1, 255)),
		"rank":            ozzo.Validate(r.Data.Attributes.Rank, ozzo.Length(1, 255)),
	}.Filter()
}
