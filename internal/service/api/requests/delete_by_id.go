package requests

import (
	"net/http"

	ozzo "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/pkg/errors"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/parameters"
)

type DeleteByIDRequest struct {
	ID int64
}

func NewDeleteByID(r *http.Request) (*DeleteByIDRequest, error) {
	id, err := parameters.FetchPathID(r)
	if err != nil {
		return nil, ozzo.Errors{
			"path {id}": errors.Wrap(err, "failed to fetch path id"),
		}
	}
	return &DeleteByIDRequest{ID: id}, nil
}
