package requests

import (
	"encoding/json"
	"net/http"

	ozzo "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/pkg/errors"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/parameters"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

type EditMasonRequest struct {
	Data    resources.EditMason
	MasonID int64
}

func NewEditMason(r *http.Request) (*EditMasonRequest, error) {
	request := new(EditMasonRequest)
	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		return nil, ozzo.Errors{
			"/": errors.Wrap(err, "failed to decode request body"),
		}
	}
	masonID, err := parameters.FetchPathID(r)
	if err != nil {
		return nil, ozzo.Errors{
			"path {id}": errors.Wrap(err, "failed to fetch path id"),
		}
	}
	request.MasonID = masonID
	return request, request.validate()

}

func (r *EditMasonRequest) validate() error {
	return ozzo.Errors{
		"new_name":       ozzo.Validate(r.Data.Attributes.NewName, ozzo.Length(1, 255)),
		"new_profession": ozzo.Validate(r.Data.Attributes.NewName, ozzo.Length(1, 255)),
		"new_rank":       ozzo.Validate(r.Data.Attributes.NewRank, ozzo.Length(1, 255)),
	}.Filter()
}
