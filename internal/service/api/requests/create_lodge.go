package requests

import (
	"encoding/json"
	"net/http"

	ozzo "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/pkg/errors"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/resources"
)

type CreateLodgeRequest struct {
	Data resources.Lodge
}

func NewCreateLodge(r *http.Request) (*CreateLodgeRequest, error) {
	request := new(CreateLodgeRequest)
	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		return nil, ozzo.Errors{
			"/": errors.Wrap(err, "failed to decode request body"),
		}
	}
	return request, request.validate()
}

func (r *CreateLodgeRequest) validate() error {
	return ozzo.Errors{
		"country": ozzo.Validate(r.Data.Attributes.Country, ozzo.Length(1, 1024)),
		"name":    ozzo.Validate(r.Data.Attributes.Name, ozzo.Length(1, 255)),
	}.Filter()
}
