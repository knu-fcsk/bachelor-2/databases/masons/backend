package api

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/config"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data/postgres"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/handlers"

	"net"
	"net/http"
)

type API interface {
	Start() error
}

type api struct {
	router   chi.Router
	listener net.Listener
	log      *logan.Entry
}

func (a *api) Start() error {
	a.log.Info("Api started on ", a.listener.Addr().String())
	return http.Serve(a.listener, a.router)
}

func NewAPI(cfg config.Config) API {
	return &api{
		router:   newRouter(cfg),
		listener: cfg.Listener(),
		log:      cfg.Log(),
	}
}

func newRouter(cfg config.Config) chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(cfg.Log()),
		ape.LoganMiddleware(cfg.Log()),
		ape.CtxMiddleware(
			ctx.SetLog(cfg.Log()),

			ctx.SetLodgesQ(postgres.NewLodgesQ(cfg.DB())),
			ctx.SetMasonsQ(postgres.NewMasonsQ(cfg.DB())),
			ctx.SetLodgeEventsQ(postgres.NewLodgeEventsQ(cfg.DB())),
			ctx.SetQuerier(postgres.NewQuerier(cfg.DB())),
		),
	)

	r.Route("/backend", func(r chi.Router) {
		r.Route("/masons", func(r chi.Router) {
			r.Get("/", handlers.GetMasons)
			r.Post("/", handlers.CreateMason)
		})
		r.Route("/masons/{id}", func(r chi.Router) {
			r.Delete("/", handlers.DeleteMason)
			r.Patch("/", handlers.EditMason)
		})

		r.Route("/lodges", func(r chi.Router) {
			r.Get("/", handlers.GetLodges)
			r.Post("/", handlers.CreateLodge)
		})
		r.Route("/lodges/{id}", func(r chi.Router) {
			r.Delete("/", handlers.DeleteLodge)
			r.Patch("/", handlers.EditLodge)
		})

		r.Route("/events", func(r chi.Router) {
			r.Get("/", handlers.GetEvents)
			r.Post("/", handlers.CreateEvent)
		})
		r.Route("/events/{id}", func(r chi.Router) {
			r.Delete("/", handlers.DeleteEvent)
			r.Patch("/", handlers.EditEvent)
		})

		r.Route("/simple-query", func(r chi.Router) {
			r.Get("/1", handlers.SimpleQuery1)
			r.Get("/2", handlers.SimpleQuery2)
			r.Get("/3", handlers.SimpleQuery3)
			r.Get("/4", handlers.SimpleQuery4)
			r.Get("/5", handlers.SimpleQuery5)
		})

		r.Route("/complex-query", func(r chi.Router) {
			r.Get("/1", handlers.ComplexQuery1)
			r.Get("/2", handlers.ComplexQuery2)
			r.Get("/3", handlers.ComplexQuery3)
		})
	})

	return r
}
