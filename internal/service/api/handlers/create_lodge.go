package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/requests"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/responses"
)

func CreateLodge(w http.ResponseWriter, r *http.Request) {
	log := ctx.Log(r)
	request, err := requests.NewCreateLodge(r)
	if err != nil {
		log.WithError(err).Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	lodge, err := ctx.LodgesQ(r).New().Create(data.Lodge{
		Name:    request.Data.Attributes.Name,
		Country: request.Data.Attributes.Country,
	})
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	response := responses.ComposeLodge(lodge)
	ape.Render(w, response)
}
