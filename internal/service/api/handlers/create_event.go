package handlers

import (
	"net/http"
	"time"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/requests"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/responses"
)

func CreateEvent(w http.ResponseWriter, r *http.Request) {
	log := ctx.Log(r)
	request, err := requests.NewCreateEvent(r)
	if err != nil {
		log.WithError(err).Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	event, err := ctx.LodgeEventsQ(r).New().Create(data.LodgeEvent{
		LodgeID:     request.Data.Attributes.LodgeId,
		Name:        request.Data.Attributes.Name,
		Date:        time.Unix(request.Data.Attributes.Date, 0),
		Description: request.Data.Attributes.Description,
	})
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	response := responses.ComposeEvent(event)
	ape.Render(w, response)
}
