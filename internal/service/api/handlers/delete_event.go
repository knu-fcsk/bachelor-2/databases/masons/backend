package handlers

import (
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data/postgres"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/requests"
)

func DeleteEvent(w http.ResponseWriter, r *http.Request) {
	log := ctx.Log(r)
	request, err := requests.NewDeleteByID(r)
	if err != nil {
		log.WithError(err).Error("invalid request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	err = ctx.LodgeEventsQ(r).New().FilterByID(request.ID).Delete()
	if errors.Is(err, postgres.ErrNoSuchLodgeEvent) {
		log.Error(err)
		ape.RenderErr(w, problems.NotFound())
		return
	}
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
