package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/parameters"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/responses"
)

func ComplexQuery2(w http.ResponseWriter, r *http.Request) {
	log := ctx.Log(r)
	k, err := parameters.FetchKParamFromHeader(r)
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	masons, err := ctx.Querier(r).ComplexQuery2(k)
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	response := responses.ComposeMasonList(masons)
	ape.Render(w, response)
}
