package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/requests"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/responses"
)

func EditLodge(w http.ResponseWriter, r *http.Request) {
	log := ctx.Log(r)
	request, err := requests.NewEditLodge(r)
	if err != nil {
		log.WithError(err).Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	lodge, err := ctx.LodgesQ(r).New().FilterByID(request.LodgeID).Get()
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if lodge == nil {
		log.Error("no such lodge")
		ape.RenderErr(w, problems.NotFound())
		return
	}
	if request.Data.Attributes.NewName != nil {
		lodge.Name = *request.Data.Attributes.NewName
	}
	if request.Data.Attributes.NewCountry != nil {
		lodge.Country = *request.Data.Attributes.NewCountry
	}
	lodge, err = ctx.LodgesQ(r).New().FilterByID(lodge.ID).Update(*lodge)
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	response := responses.ComposeLodge(lodge)
	ape.Render(w, response)
}
