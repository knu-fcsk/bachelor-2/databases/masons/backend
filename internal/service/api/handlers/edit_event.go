package handlers

import (
	"net/http"
	"time"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/requests"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/responses"
)

func EditEvent(w http.ResponseWriter, r *http.Request) {
	log := ctx.Log(r)
	request, err := requests.NewEditEvent(r)
	if err != nil {
		log.WithError(err).Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	event, err := ctx.LodgeEventsQ(r).New().FilterByID(request.EventID).Get()
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if event == nil {
		log.Error("no such event")
		ape.RenderErr(w, problems.NotFound())
		return
	}
	if request.Data.Attributes.NewDate != nil {
		event.Date = time.Unix(*request.Data.Attributes.NewDate, 0)
	}
	if request.Data.Attributes.NewDescription != nil {
		event.Description = *request.Data.Attributes.NewDescription
	}
	if request.Data.Attributes.NewName != nil {
		event.Name = *request.Data.Attributes.NewName
	}
	if request.Data.Attributes.NewLodgeId != nil {
		event.LodgeID = *request.Data.Attributes.NewLodgeId
	}
	event, err = ctx.LodgeEventsQ(r).New().FilterByID(event.ID).Update(*event)
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	response := responses.ComposeEvent(event)
	ape.Render(w, response)
}
