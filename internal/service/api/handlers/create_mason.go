package handlers

import (
	"net/http"
	"time"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/requests"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/responses"
)

func CreateMason(w http.ResponseWriter, r *http.Request) {
	log := ctx.Log(r)
	request, err := requests.NewCreateMason(r)
	if err != nil {
		log.WithError(err).Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	mason, err := ctx.MasonsQ(r).New().Create(data.Mason{
		LodgeID:        request.Data.Attributes.LodgeId,
		Name:           request.Data.Attributes.Name,
		Rank:           request.Data.Attributes.Rank,
		Profession:     request.Data.Attributes.Profession,
		AcceptanceDate: time.Unix(request.Data.Attributes.AcceptanceDate, 0),
	})
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	response := responses.ComposeMason(mason)
	ape.Render(w, response)
}
