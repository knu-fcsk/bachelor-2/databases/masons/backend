package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/parameters"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/responses"
)

func SimpleQuery2(w http.ResponseWriter, r *http.Request) {
	log := ctx.Log(r)
	n, err := parameters.FetchNParamFromHeader(r)
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	k, err := parameters.FetchKParamFromHeader(r)
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	lodges, err := ctx.Querier(r).SimpleQuery2(n, k)
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	response := responses.ComposeLodgeList(lodges)
	ape.Render(w, response)
}
