package handlers

import (
	"net/http"
	"time"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/ctx"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/requests"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api/responses"
)

func EditMason(w http.ResponseWriter, r *http.Request) {
	log := ctx.Log(r)
	request, err := requests.NewEditMason(r)
	if err != nil {
		log.WithError(err).Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	mason, err := ctx.MasonsQ(r).New().FilterByID(request.MasonID).Get()
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if mason == nil {
		log.Error("no such mason")
		ape.RenderErr(w, problems.NotFound())
		return
	}
	if request.Data.Attributes.NewName != nil {
		mason.Name = *request.Data.Attributes.NewName
	}
	if request.Data.Attributes.NewProfession != nil {
		mason.Profession = *request.Data.Attributes.NewProfession
	}
	if request.Data.Attributes.NewRank != nil {
		mason.Rank = *request.Data.Attributes.NewRank
	}
	if request.Data.Attributes.NewLodgeId != nil {
		mason.LodgeID = *request.Data.Attributes.NewLodgeId
	}
	if request.Data.Attributes.NewAcceptanceDate != nil {
		mason.AcceptanceDate = time.Unix(*request.Data.Attributes.NewAcceptanceDate, 0)
	}
	mason, err = ctx.MasonsQ(r).New().FilterByID(mason.ID).Update(*mason)
	if err != nil {
		log.Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	response := responses.ComposeMason(mason)
	ape.Render(w, response)
}
