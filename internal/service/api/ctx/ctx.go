package ctx

import (
	"context"
	"net/http"

	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/data"

	"gitlab.com/distributed_lab/logan/v3"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota + 1
	masonsQCtxKey
	lodgesQCtxKey
	lodgeEventsQCtxKey
	querierCtxKey
)

func SetLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func SetLodgesQ(q data.Lodges) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, lodgesQCtxKey, q)
	}
}

func LodgesQ(r *http.Request) data.Lodges {
	return r.Context().Value(lodgesQCtxKey).(data.Lodges)
}

func SetMasonsQ(q data.Masons) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, masonsQCtxKey, q)
	}
}

func MasonsQ(r *http.Request) data.Masons {
	return r.Context().Value(masonsQCtxKey).(data.Masons)
}

func SetLodgeEventsQ(q data.LodgeEvents) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, lodgeEventsQCtxKey, q)
	}
}

func LodgeEventsQ(r *http.Request) data.LodgeEvents {
	return r.Context().Value(lodgeEventsQCtxKey).(data.LodgeEvents)
}

func SetQuerier(q data.Querier) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, querierCtxKey, q)
	}
}

func Querier(r *http.Request) data.Querier {
	return r.Context().Value(querierCtxKey).(data.Querier)
}
