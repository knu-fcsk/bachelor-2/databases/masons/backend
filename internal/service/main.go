package service

import (
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/config"
	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/service/api"
)

type service struct {
	log *logan.Entry
	api api.API
}

func Run(cfg config.Config) {
	if err := newService(cfg).run(); err != nil {
		panic(err)
	}
}

func newService(cfg config.Config) *service {
	return &service{
		log: cfg.Log(),
		api: api.NewAPI(cfg),
	}
}

func (s *service) run() error {
	s.log.Info("Service started")
	return s.api.Start()
}
