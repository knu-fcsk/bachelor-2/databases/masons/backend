-- +migrate Up

CREATE TABLE lodges(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    country VARCHAR(255) NOT NULL
);

CREATE TABLE masons(
    id SERIAL PRIMARY KEY,
    lodge_id INTEGER REFERENCES lodges (id) ON DELETE CASCADE,
    name VARCHAR(255) NOT NULL,
    rank VARCHAR(255) NOT NULL,
    profession VARCHAR(255) NOT NULL,
    acceptance_date DATE NOT NULL
);

CREATE TABLE lodge_events(
    id SERIAL PRIMARY KEY,
    lodge_id INTEGER REFERENCES lodges(id) ON DELETE CASCADE,
    name VARCHAR(255) NOT NULL,
    date DATE NOT NULL,
    description VARCHAR(1024) NOT NULL
);

CREATE TABLE events_members(
    id SERIAL PRIMARY KEY,
    event_id INTEGER NOT NULL REFERENCES lodge_events(id) ON DELETE CASCADE,
    mason_id INTEGER NOT NULL REFERENCES masons(id) ON DELETE CASCADE
);

-- +migrate Down

DROP TABLE IF EXISTS events_members;
DROP TABLE IF EXISTS lodge_events;
DROP TABLE IF EXISTS masons;
DROP TABLE IF EXISTS lodges;