FROM golang:1.18-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend
COPY vendor .
COPY . .

RUN GOOS=linux go build  -o /usr/local/bin/backend /go/src/gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/backend /usr/local/bin/backend
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["backend"]
