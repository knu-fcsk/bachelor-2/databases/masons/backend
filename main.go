package main

import (
	"os"

	"gitlab.com/knu-fcsk/bachelor-2/databases/masons/lab2/backend/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
