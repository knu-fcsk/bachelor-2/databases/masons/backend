/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type EditMasonAttributes struct {
	NewAcceptanceDate *int64  `json:"new_acceptance_date,omitempty"`
	NewLodgeId        *int64  `json:"new_lodge_id,omitempty"`
	NewName           *string `json:"new_name,omitempty"`
	NewProfession     *string `json:"new_profession,omitempty"`
	NewRank           *string `json:"new_rank,omitempty"`
}
