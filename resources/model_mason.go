/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Mason struct {
	Key
	Attributes MasonAttributes `json:"attributes"`
}
type MasonResponse struct {
	Data     Mason    `json:"data"`
	Included Included `json:"included"`
}

type MasonListResponse struct {
	Data     []Mason  `json:"data"`
	Included Included `json:"included"`
	Links    *Links   `json:"links"`
}

// MustMason - returns Mason from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustMason(key Key) *Mason {
	var mason Mason
	if c.tryFindEntry(key, &mason) {
		return &mason
	}
	return nil
}
