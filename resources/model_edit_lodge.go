/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type EditLodge struct {
	Key
	Attributes EditLodgeAttributes `json:"attributes"`
}
type EditLodgeResponse struct {
	Data     EditLodge `json:"data"`
	Included Included  `json:"included"`
}

type EditLodgeListResponse struct {
	Data     []EditLodge `json:"data"`
	Included Included    `json:"included"`
	Links    *Links      `json:"links"`
}

// MustEditLodge - returns EditLodge from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustEditLodge(key Key) *EditLodge {
	var editLodge EditLodge
	if c.tryFindEntry(key, &editLodge) {
		return &editLodge
	}
	return nil
}
