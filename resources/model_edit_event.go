/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type EditEvent struct {
	Key
	Attributes EditEventAttributes `json:"attributes"`
}
type EditEventResponse struct {
	Data     EditEvent `json:"data"`
	Included Included  `json:"included"`
}

type EditEventListResponse struct {
	Data     []EditEvent `json:"data"`
	Included Included    `json:"included"`
	Links    *Links      `json:"links"`
}

// MustEditEvent - returns EditEvent from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustEditEvent(key Key) *EditEvent {
	var editEvent EditEvent
	if c.tryFindEntry(key, &editEvent) {
		return &editEvent
	}
	return nil
}
