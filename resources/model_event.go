/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Event struct {
	Key
	Attributes EventAttributes `json:"attributes"`
}
type EventResponse struct {
	Data     Event    `json:"data"`
	Included Included `json:"included"`
}

type EventListResponse struct {
	Data     []Event  `json:"data"`
	Included Included `json:"included"`
	Links    *Links   `json:"links"`
}

// MustEvent - returns Event from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustEvent(key Key) *Event {
	var event Event
	if c.tryFindEntry(key, &event) {
		return &event
	}
	return nil
}
