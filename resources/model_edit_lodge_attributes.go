/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type EditLodgeAttributes struct {
	NewCountry *string `json:"new_country,omitempty"`
	NewName    *string `json:"new_name,omitempty"`
}
