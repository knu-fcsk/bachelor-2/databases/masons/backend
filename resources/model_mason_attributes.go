/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type MasonAttributes struct {
	AcceptanceDate int64  `json:"acceptance_date"`
	LodgeId        int64  `json:"lodge_id"`
	Name           string `json:"name"`
	Profession     string `json:"profession"`
	Rank           string `json:"rank"`
}
