/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type EditMason struct {
	Key
	Attributes EditMasonAttributes `json:"attributes"`
}
type EditMasonResponse struct {
	Data     EditMason `json:"data"`
	Included Included  `json:"included"`
}

type EditMasonListResponse struct {
	Data     []EditMason `json:"data"`
	Included Included    `json:"included"`
	Links    *Links      `json:"links"`
}

// MustEditMason - returns EditMason from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustEditMason(key Key) *EditMason {
	var editMason EditMason
	if c.tryFindEntry(key, &editMason) {
		return &editMason
	}
	return nil
}
