/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type EventAttributes struct {
	Date        int64  `json:"date"`
	Description string `json:"description"`
	LodgeId     int64  `json:"lodge_id"`
	Name        string `json:"name"`
}
