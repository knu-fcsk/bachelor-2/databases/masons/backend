/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Lodge struct {
	Key
	Attributes LodgeAttributes `json:"attributes"`
}
type LodgeResponse struct {
	Data     Lodge    `json:"data"`
	Included Included `json:"included"`
}

type LodgeListResponse struct {
	Data     []Lodge  `json:"data"`
	Included Included `json:"included"`
	Links    *Links   `json:"links"`
}

// MustLodge - returns Lodge from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustLodge(key Key) *Lodge {
	var lodge Lodge
	if c.tryFindEntry(key, &lodge) {
		return &lodge
	}
	return nil
}
