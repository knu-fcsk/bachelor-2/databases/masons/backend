/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type EditEventAttributes struct {
	NewDate        *int64  `json:"new_date,omitempty"`
	NewDescription *string `json:"new_description,omitempty"`
	NewLodgeId     *int64  `json:"new_lodge_id,omitempty"`
	NewName        *string `json:"new_name,omitempty"`
}
