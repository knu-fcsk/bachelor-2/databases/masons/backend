/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ResourceType string

// List of ResourceType
const (
	EDIT_EVENT ResourceType = "edit_event"
	EDIT_LODGE ResourceType = "edit_lodge"
	EDIT_MASON ResourceType = "edit_mason"
	EVENT      ResourceType = "event"
	LODGE      ResourceType = "lodge"
	MASON      ResourceType = "mason"
)
