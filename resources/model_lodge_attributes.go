/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type LodgeAttributes struct {
	Country string `json:"country"`
	Name    string `json:"name"`
}
